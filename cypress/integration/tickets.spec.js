describe("Tickets", () => {

    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"))

    it("Fills all the text input fields", () => {
        const firstName = "Guilherme"
        const lastName = "Neves"

        cy.get("#first-name").type(firstName)
        cy.get("#last-name").type(lastName)
        cy.get("#email").type("guilherme@teste.com")
        cy.get("#requests").type("Vegetarian")
        cy.get("#signature").type(`${firstName} ${lastName}`)

    })

    it("Select two tickets", () => {
        cy.get("#ticket-quantity").select("2")
    })

    it("Select vip ticket type", () => {
        cy.get("#vip").check()
    })

    it("Selects social media checkbox", () => {
        cy.get("#social-media").check()
    })

    it("Selects fired, publication, then uncheck friend checkbox", () => {
        cy.get("#friend").check()
        cy.get("#publication").check()
        cy.get("#friend").uncheck()
    })

    it("Has 'TICKETBOX' header's heading", () => {
        cy.get("header img + h1").should("contain", "TICKETBOX")
    })

    it("Alerts on invalid email", () => {
        cy.get("#email")
            .as("email")
            .type("teste-teste.com")

        cy.get("#email.invalid")
            .as("invalidEmail")
            .should("exist")

        cy.get("@email")
            .clear()
            .type("guilherme@teste.com")

        cy.get("#email.invalid").should("not.exist")
    })

    it("Fills abd reset the form", () => {
        const firstName = "Guilherme"
        const lastName = "Neves"
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName)
        cy.get("#last-name").type(lastName)
        cy.get("#email").type("guilherme@teste.com")
        cy.get("#ticket-quantity").select("2")
        cy.get("#vip").check()
        cy.get("#friend").check()
        cy.get("#requests").type("IPA beer")

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        )

        cy.get("#agree").click()
        cy.get("#signature").type(fullName)

        cy.get("button[type=submit]")
            .as("submitButton")
            .should("not.be.disabled")
        
        cy.get("button[type=reset]").click()

        cy.get("@submitButton").should("be.disabled")
    })

    it("Fills mandatory fields using support command", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@example.com"
        }

        cy.fillMandatoryFields(customer)

        cy.get("button[type=submit]")
            .as("submitButton")
            .should("not.be.disabled")
        
        cy.get("#agree").uncheck()

        cy.get("@submitButton").should("be.disabled")

    })
})